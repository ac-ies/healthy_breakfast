Base Cpp
--------
Base app that includes SFML & GTest suite.

Make Targets
------------
make - make app
make run - run app
make clean - clean build
make test - make & run test
make restclean - clean test build
