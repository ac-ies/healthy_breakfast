APPNAME := healthy

ARCH := $(shell getconf LONG_BIT)

CXX		:= clang++
CXXFLAGS:= -Iinc -Wall -W -std=c++14 -g -pedantic 
LFLAGS := -Llib/$(ARCH) 
LIBS	:= 
SRCDIR  := src
BINDIR	:= bin
BUILDDIR:= build
ENTRY	:= main.cc

SRCS=$(shell find $(SRCDIR) -maxdepth 1 -name "*.cc")

OBJ=$(SRCS:%.cc=$(BUILDDIR)/%.o)
DEP=$(OBJ:%.o=%.d)

all: $(APPNAME)

.PHONY : run
run: $(APPNAME)
	./$(BINDIR)/$(APPNAME)

clean: testclean
	rm -rf $(BINDIR) $(OBJ) $(DEP) $(BUILDDIR)

$(APPNAME): $(BINDIR)/$(APPNAME)

$(BINDIR)/$(APPNAME): $(OBJ)
	mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(LFLAGS) -o $@ $(OBJ) $(LIBS)

$(BUILDDIR)/%.o : %.cc
	mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) -c -MMD -MP $< -o $@ 

-include $(DEP)

# -----===== Testing =====----- #

T_DIR := test
T_BUILDDIR = $(T_DIR)/$(BUILDDIR)
T_LFLAGS := $(LFLAGS)
T_FLAGS := $(CXXFLAGS) -I$(T_DIR)/inc
T_LIBS := -lgtest_main -lgtest -lpthread $(LIBS)

T_SRCS := $(shell find $(SRCDIR) -maxdepth 1 -name "*.cc" ! -name $(ENTRY)) $(shell find $(T_DIR) -maxdepth 1 -name "*.cc")
T_OBJ := $(T_SRCS:%.cc=$(T_BUILDDIR)/%.o)
T_DEP := $(T_OBJ:%.o=%.d)

.PHONY : testclean
testclean: 
	rm -rf $(T_BUILDDIR)
	rm -f $(T_DIR)/tests

.PHONY : test
test: $(T_DIR)/tests
	clear && GTEST_COLOR=1 ./$(T_DIR)/tests

$(T_DIR)/tests: $(T_OBJ) 
	mkdir -p $(@D)
	$(CXX) $(T_FLAGS) $(T_LFLAGS) -o $@ $(T_OBJ) $(T_LIBS)
	
$(T_BUILDDIR)/%.o : %.cc
	mkdir -p $(@D)
	$(CXX) $(T_FLAGS) -c -MMD -MP $< -o $@ 

-include $(T_DEP)
