#pragma once

#include <string>
#include <vector>

namespace healthy_breakfast {

class encoder {
    public:
        std::string to_binary(const std::string& serial);
        std::string to_hex(const std::string& serial);
        std::vector<std::string> to_fruit(const std::string& serial);

        int         convertBeamerTypeToBit(const std::string& beamer);
        std::string convertHardwareVersionToBit(const std::string& hardwareVersion);
        std::string convertFlavourToBit(const std::string& flavour);
        std::string convertUnitNumberToBit(const std::string& unitNumber);
        std::string convertYearNumberToBit(const std::string& yearNumber);
        int         convertPostFixToBit(const std::string& postFix);

        std::string extractFlavourFromSerial(const std::string& serialNumber);

        static std::string int_to_binary(int n, int digits = 0)
        {
            std::string r;
            while(n!=0) {r=(n%2==0 ?"0":"1")+r; n/=2;}
            if(digits > 0)
                while(r.size() < digits) { r = "0" + r; }
            return r;
        }
};

}
